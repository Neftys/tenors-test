# Tenors-Test

Projet permettant de tester les frameworks Remix et Tailwind CSS.  
Réalisation d'une application web gérant des musiciens et des concerts.

# Lancement

- Cloner le projet à partir de l'adresse https://gitlab.com/Neftys/tenors-test.
- Lancer la commande ``npm install``.
- Exécuter la commande ``npm run dev``.

# Versions utilisées

- Node : 16.14.2
- NPM : 8.19.2
- React : 18.2.0
- Remix : 1.7.4
- Tailwind : 3.2.1

## CREDITS
<a href="https://www.flaticon.com/fr/icones-gratuites/chat" title="chat icônes">Chat icônes créées par Darius Dan - Flaticon</a>