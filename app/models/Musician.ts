import type {Instruments} from "~/models/Instruments";
import type {MusicStyle} from "~/models/MusicStyle";

/**
 * Simulate a musician with his information.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
export type Musician = {

    /**
     * Musician identifier. Must be unique.
     */
    id: string;

    /**
     * Musician's stage name.
     */
    name: string;

    /**
     * List of instruments played by the musician.
     */
    instruments: Instruments[];

    /**
     * List of music styles played by the musician.
     */
    styles: MusicStyle[];
}
