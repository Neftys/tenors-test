import type {MusicStyle} from "~/models/MusicStyle";
import {City} from "~/models/City";

/**
 * Simulates a concert with its information.
 * @author Krysalizys
 * @creation_date 04/11/2022
 */
export type Concert = {

    /**
     * Concert identifier. Must be unique.
     */
    id: string;
    /**
     * Style of music played during the concert.
     */
    style: MusicStyle;
    /**
     * Street name.
     */
    street: string;
    /**
     * City name.
     */
    city: City;
    /**
     * Concert date.
     */
    date: Date;
}
