/**
 * Enumeration including some music styles.
 * @enum MusicStyle
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
export enum MusicStyle {
    POP,
    ROCK,
    ELECTRONIC,
    CLASSIC,
    METAL,
    ALTERNATIVE
}

export const READABLE_MUSIC_STYLE = [
    "Pop",
    "Rock",
    "Electro",
    "Classique",
    "Métal",
    "Alternatif"
]