/**
 * Enumeration including some musical instruments.
 * @enum Instruments
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
export enum Instruments {
    VIOLIN,
    BASS,
    PIANO,
    DRUMS,
    ACCORDIONS,
    FLUTE,
}

export const READABLE_INSTRUMENTS = [
    "Violon",
    "Basse",
    "Piano",
    "Batterie",
    "Accordéon",
    "Flûte"
]