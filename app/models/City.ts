/**
 * Enumeration with random city names used to create addresses.
 * @enum City
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
export enum City {
    WAELDESTONE,
    BRIAR,
    GLEN,
    LARCBOST,
    ASHBOURNE,
    SAKER,
    KEEP,
    BROKEN,
    SHIELD,
    AERILON,
    CRAYDON,
    SNOWBUSH,
    SWINDON,
}

export const READABLE_CITIES = [
    "Waeldestone",
    "Briar",
    "Glen",
    "Larcbost",
    "Ashbourne",
    "Saker",
    "Keep",
    "Broken",
    "Shield",
    "Aerilon",
    "Craydon",
    "Snowbush",
    "Swindon",
];