import React from "react";
import EditMusicianPage from "~/components/pages/EditMusicianPage";
import {ActionFunction, json, LoaderFunction, redirect} from "@remix-run/node";
import MusicianService, {getMusician} from "~/services/MusicianService";


export default function EditMusicianRoute() {
    return (
        <EditMusicianPage/>
    );
}

export type LoaderData = {
    musician: Awaited<ReturnType<typeof getMusician>>
}

/**
 * Retrieves and stores all musicians when the page is called.
 */
export const loader: LoaderFunction = async ({params}) => {
    if(params.musicianId) {
        return json<LoaderData>({
            musician: await getMusician(params.musicianId)
        });
    }
    else {
        return null;
    }
};

/**
 * Function called when the user submits a validation of the form present on the "/edit-musician/$id" page.
 * Comes from a POST request and allows to process the form.
 * @param request
 */
export const action: ActionFunction = async ({request}) => {
    // Retrieving the form
    const formData = await request.formData();
    // Generates musician
    const newMusician = MusicianService.getMusicianFromFormData(formData);
    if(newMusician) {
        // Add musician
        MusicianService.editMusician(newMusician);
    }
    return redirect(`/musicians/all`);
};
