import MusicianPage from "~/components/pages/MusicianPage";
import {json, LoaderFunction} from "@remix-run/node";
import {getMusicians} from "../../services/MusicianService";

/**
 *
 * @author Krysalizys
 * @creation_date 02/11/2022
 **/
export default function MusicianRoute() {
    return (
        <MusicianPage/>
    );
}

export type LoaderData = {
    musicians: Awaited<ReturnType<typeof getMusicians>>
}

/**
 * Retrieves and stores all Musicians when the page is called.
 */
export const loader: LoaderFunction = async ({request}) => {
    const url = new URL(request.url);
    const style = url.searchParams.get("music-style");
    const instrument = url.searchParams.get("instrument");
    return json<LoaderData>({
        musicians: await getMusicians(style, instrument)
    });
};

