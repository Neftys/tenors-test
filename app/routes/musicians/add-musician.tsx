import AddMusicianPage from "~/components/pages/AddMusicianPage";
import {ActionFunction, redirect} from "@remix-run/node";
import MusicianService from "~/services/MusicianService";

/**
 * Page displayed when the route "/add-musician" is used.
 * @author Krysalizys
 * @creation_date 02/11/2022
 **/
export default function AddMusicianRoute() {
    return (
        <AddMusicianPage/>
    );
}

/**
 * Function called when the user submits a validation of the form present on the "/add-musician" page.
 * Comes from a POST request and allows to process the form.
 * @param request
 */
export const action: ActionFunction = async ({request}) => {
    // Retrieving the form
    const formData = await request.formData();
    // Generates musician
    const newMusician = MusicianService.getMusicianFromFormData(formData);
    if(newMusician) {
        // Add musician
        MusicianService.addMusician(newMusician);
    }
    return redirect(`/musicians/all`);
}
