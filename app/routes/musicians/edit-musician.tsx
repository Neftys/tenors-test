import {Outlet} from "react-router";

/**
 * Page displayed when the route "/add-musician" is used.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
export default function IndexEditMusicianRoute() {
    return (
        <Outlet/>
    );
}
