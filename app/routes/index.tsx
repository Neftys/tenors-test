/**
 *
 * @author Krysalizys
 * @creation_date 02/11/2022
 **/
export default function IndexRoute() {
  return (
      <div className="w-full text-center pt-16">
          <h1 className="font-bold text-indigo-700 text-9xl tracking-widest">
              Bienvenue !
          </h1>
      </div>
  );
}
