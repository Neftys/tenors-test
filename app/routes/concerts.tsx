import {Outlet} from "react-router";


/**
 *
 * @author Krysalizys
 * @creation_date 02/11/2022
 **/
export default function ConcertIndexRoute() {
    return (
        <Outlet/>
    );
}

