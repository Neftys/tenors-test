import {ActionFunction, redirect} from "@remix-run/node";
import ConcertService from "~/services/ConcertService";

/**
 * Page displayed when the route "/delete-concert" is used.
 * @author Krysalizys
 * @creation_date 06/11/2022
 **/
export default function DeleteConcertRoute() {
    return (
        <h1>Suppression en cours</h1>
    );
}

/**
 * Function called when the user click on delete concert.
 * Comes from a POST request and allows to process the form.
 * @param request
 */
export const action: ActionFunction = async ({request}) => {
    // Retrieving the form
    const formData = await request.formData();
    // Retrieving values
    let id = formData.get("concert-id");
    // Delete a concert.
    if(id !== null) {
        let isDelete = ConcertService.deleteConcert(id.toString());
        if(!isDelete) {
            console.log("Not deleted");
        }
    }
    return redirect(`./all`);
};
