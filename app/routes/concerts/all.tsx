import ConcertPage from "~/components/pages/ConcertPage";
import {ActionFunction, json, LoaderFunction, redirect} from "@remix-run/node";
import ConcertService, {getFuturesConcerts, getPastConcerts} from "../../services/ConcertService";

/**
 *
 * @author Krysalizys
 * @creation_date 02/11/2022
 **/
export default function ConcertRoute() {
    return (
        <ConcertPage/>
    );
}

export type LoaderData = {
    futuresConcerts: Awaited<ReturnType<typeof getFuturesConcerts>>,
    pastConcerts: Awaited<ReturnType<typeof getFuturesConcerts>>
}

/**
 * Retrieves and stores all concerts when the page is called.
 */
export const loader: LoaderFunction = async ({request}) => {
    const url = new URL(request.url);
    const style = url.searchParams.get("music-style");
    return json<LoaderData>({
        futuresConcerts: await getFuturesConcerts(style),
        pastConcerts: await getPastConcerts(style)
    });
};

