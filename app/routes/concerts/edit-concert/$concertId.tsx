import React from "react";
import EditConcertPage from "~/components/pages/EditConcertPage";
import {ActionFunction, json, LoaderFunction, redirect} from "@remix-run/node";
import ConcertService, {getConcert} from "~/services/ConcertService";

export default function EditConcertRoute() {
    return (
        <EditConcertPage/>
    );
}

export type LoaderData = {
    concert: Awaited<ReturnType<typeof getConcert>>
}

/**
 * Retrieves and stores all concerts when the page is called.
 */
export const loader: LoaderFunction = async ({params}) => {
    if(params.concertId) {
        return json<LoaderData>({
            concert: await getConcert(params.concertId)
        });
    }
    else {
        return null;
    }
};

/**
 * Function called when the user submits a validation of the form present on the "/edit-concert/$id" page.
 * Comes from a POST request and allows to process the form.
 * @param request
 */
export const action: ActionFunction = async ({request}) => {
    // Retrieving the form
    const formData = await request.formData();
    // Generates concert
    const newConcert = ConcertService.getConcertFromFormData(formData);
    if(newConcert) {
        // Add concert
        ConcertService.editConcert(newConcert);
    }
    return redirect(`/concerts/all`);
};
