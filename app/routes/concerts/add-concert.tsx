import {ActionFunction, redirect} from "@remix-run/node";
import ConcertService from "~/services/ConcertService";
import AddConcertPage from "~/components/pages/AddConcertPage";

/**
 * Page displayed when the route "/add-concert" is used.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
export default function AddConcertRoute() {
    return (
        <AddConcertPage/>
    );
}

/**
 * Function called when the user submits a validation of the form present on the "/add-concert" page.
 * Comes from a POST request and allows to process the form.
 * @param request
 */
export const action: ActionFunction = async ({request}) => {
    // Retrieving the form
    const formData = await request.formData();
    // Generates concert
    const newConcert = ConcertService.getConcertFromFormData(formData);
    if(newConcert) {
        // Add concert
        ConcertService.addConcert(newConcert);
    }
    return redirect(`/concerts/all`);
}
