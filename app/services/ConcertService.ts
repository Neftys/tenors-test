import data from "~/data/ConcertsData.json";
import type {Concert} from "~/models/Concert";
import fs from "fs";
/**
 *
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class ConcertService {

    /**
     * Retrieves the concerts' data stored in a JSON.
     */
    public getConcertsData() {
        return data;
    }

    /**
     * Scans the JSON data of concerts and transforms them into objects of the right type.
     * @return Concert[] Returns an array of objects Concert.
     */
    public getAllConcerts(): Concert[] {
        const data = this.getConcertsData();
        const concerts: Concert[] = [];
        data.map((concert) => {
            let currentConcert: Concert = {
                id: concert.id,
                style: concert.style,
                street: concert.street,
                city: concert.city,
                date: new Date(concert.date)
            };
            concerts.push(currentConcert);
            return currentConcert;
        });
        return concerts;
    }

    /**
     * Adds a concert to the existing list.
     * @param concert Concert to add.
     */
    public addConcert(concert: Concert) {
        const concerts = this.getAllConcerts();
        concerts.push(concert);
        this.saveConcerts(concerts);
    }

    /**
     * Allows you to remove a concert from the existing list.
     * @param concertId Concert's identifier to be deleted.
     * @return True if the concert has been deleted, false if it does not exist or if the deletion has failed.
     */
    public deleteConcert(concertId: string): boolean {
        const concerts = this.getAllConcerts();
        let index = -1;
        for(let i = 0; i < concerts.length; i++) {
            if(concerts[i].id === concertId) {
                index = i;
            }
        }
        if(index !== -1) {
            concerts.splice(index, 1);
            this.saveConcerts(concerts);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds a concert in the list according to his ID.
     * @param concertId Identifier of the concert to find.
     * @return Returns the concert if it exists, returns null otherwise.
     */
    public getConcertById(concertId: string) {
        const concerts = this.getAllConcerts();
        for(let i = 0; i < concerts.length; i++) {
            if(concerts[i].id === concertId) {
                return concerts[i];
            }
        }
        return null;
    }

    /**
     * Modifies the concert passed in parameter if it exists.
     * @param concert Updated concert.
     */
    public editConcert(concert: Concert) {
        const concerts = this.getAllConcerts();
        let index = -1;
        for(let i = 0; i < concerts.length; i++) {
            if(concerts[i].id === concert.id) {
                index = i;
                break;
            }
        }
        if(index !== -1) {
            concerts[index] = concert;
            this.saveConcerts(concerts);
        }
    }

    /**
     * Saves a list of concerts in the JSON file provided for this purpose.
     * @param concerts List to be saved.
     */
    private saveConcerts(concerts: Concert[]) {
        fs.writeFile('./app/data/ConcertsData.json', JSON.stringify(concerts), (err) => {
            console.log(err);
        });
    }

    /**
     * Create a concert object from the information in a form.
     * @param formData Completed form
     * @return Return a new concert if the information allows it. Returns null otherwise.
     */
    public getConcertFromFormData(formData: FormData): Concert | null {
        let newConcert = null;
        // Retrieving values
        let dateData = formData.get("date");
        let streetData = formData.get("street");
        let cityData = formData.get("city");
        let styleData = formData.get("music-style");
        let idData = formData.get("concert-id");

        // Creates an id if there is not
        if(idData === null) {
            idData = new Date().getTime().toString();
        }
        // Creates a concert and adds him to the list if all fields are filled in.
        if(
            dateData !== null &&
            streetData !== null &&
            cityData !== null &&
            styleData !== null
        ) {
            const date = new Date(dateData.toString());
            const street = streetData.toString();
            const city = parseInt(cityData.toString());
            const style = parseInt(styleData.toString());
            newConcert = {
                id: idData.toString(),
                style: style,
                street: street,
                city: city,
                date: date
            };
        }
        return newConcert;
    }

    public getSortedConcerts(style: string | null) {
        // Get all concerts
        let allConcerts = this.getAllConcerts();
        if(style) {
            allConcerts = allConcerts.filter((concert) => (parseInt(style) === concert.style));
        }
        // Sort the concerts to display them in order
        return allConcerts.sort((a,b)=>a.date.getTime()-b.date.getTime());
    }
}

export default new ConcertService();

export async function getFuturesConcerts(style: string | null): Promise<Array<Concert>> {
    const service = new ConcertService();
    // Filter the concerts to remove those that have already taken place.
    return service.getSortedConcerts(style).filter((concert) => (
        concert.date > new Date()
    ));
}

export async function getPastConcerts(style: string | null): Promise<Array<Concert>> {
    const service = new ConcertService();
    // Filter the concerts to remove those that have already taken place.
    return service.getSortedConcerts(style).filter((concert) => (
        concert.date < new Date()
    ));
}

export async function getConcert(id: string): Promise<Concert | null> {
    const service = new ConcertService();
    return service.getConcertById(id);
}

