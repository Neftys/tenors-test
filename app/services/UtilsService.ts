/**
 *
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class UtilsService {

    public formatMonth(month: number): string {
        const realMonth = month + 1;
        return realMonth < 10 ? ("0" + realMonth.toString()) : realMonth.toString();
    }

    public formatDateWithoutTime(date: Date): string {
        const correctDate = new Date(date);
        return (
            correctDate.getDate() + "/" + this.formatMonth(correctDate.getMonth()) + "/" + correctDate.getFullYear()
        );
    }

    public formatTime(formattedDate: Date): string {
        const date = new Date(formattedDate);
        // Format hours
        let dateHours = date.getHours().toString();
        let hours = "0";
        if(dateHours.length === 1) {
            hours += dateHours;
        } else {
            hours = dateHours;
        }

        // Format minutes
        let dateMinutes = date.getMinutes().toString();
        let minutes = "0";
        if(dateMinutes.length === 1) {
            minutes += dateMinutes;
        } else {
            minutes = dateMinutes;
        }

        return (
            hours + ":" + minutes
        );
    }

    public formatDateForInput(formattedDate: Date) {
        const date = new Date(formattedDate);
        let month = this.formatMonth(date.getMonth());
        let day = "0";
        const dayDate = date.getDate().toString();
        if(dayDate.length === 1) {
            day += dayDate;
        } else {
            day = dayDate;
        }

        return (
            date.getFullYear() + "-" +
            month + "-" +
            day + "T" +
            this.formatTime(date)
        );
    }

}

export default new UtilsService();