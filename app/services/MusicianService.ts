import data from '../data/MusiciansData.json';
import {Musician} from "~/models/Musician";
import * as fs from "fs";
import {Instruments} from "~/models/Instruments";
import {MusicStyle} from "~/models/MusicStyle";

/**
 * Service containing various functions concerning the musicians' data.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
class MusicianService {

    /**
     * Retrieves the musicians' data stored in a JSON.
     */
    public getMusiciansData() {
        return data;
    }

    /**
     * Scans the JSON data of musicians and transforms them into objects of the right type.
     * @return Musician[] Returns an array of objects Musician.
     */
    public getAllMusicians(): Musician[] {
        const data = this.getMusiciansData();
        const musicians: Musician[] = [];
        data.map((musician) => {
            let currentMusician = musician as Musician;
            musicians.push(currentMusician);
            return currentMusician;
        });
        return musicians;
    }

    /**
     * Adds a musician to the existing list.
     * @param musician Musician to add.
     */
    public addMusician(musician: Musician) {
        const musicians = this.getAllMusicians();
        musicians.push(musician);
        this.saveMusicians(musicians);
    }

    /**
     * Allows you to remove a musician from the existing list.
     * @param musicianId Musician's identifier to be deleted.
     * @return True if the musician has been deleted, false if it does not exist or if the deletion has failed.
     */
    public deleteMusician(musicianId: string): boolean {
        const musicians = this.getAllMusicians();
        let index = -1;
        for(let i = 0; i < musicians.length; i++) {
            if(musicians[i].id === musicianId) {
                index = i;
            }
        }
        if(index !== -1) {
            musicians.splice(index, 1);
            this.saveMusicians(musicians);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds a musician in the list according to his ID.
     * @param musicianId Identifier of the musician to find.
     * @return Returns the musician if it exists, returns null otherwise.
     */
    public getMusicianById(musicianId: string) {
        const musicians = this.getAllMusicians();
        for(let i = 0; i < musicians.length; i++) {
            if(musicians[i].id === musicianId) {
                return musicians[i];
            }
        }
        return null;
    }

    /**
     * Modifies the musician passed in parameter if it exists.
     * @param musician Updated musician.
     */
    public editMusician(musician: Musician) {
        const musicians = this.getAllMusicians();
        let index = -1;
        for(let i = 0; i < musicians.length; i++) {
            if(musicians[i].id === musician.id) {
                index = i;
                break;
            }
        }
        if(index !== -1) {
            musicians[index] = musician;
            this.saveMusicians(musicians);
        }
    }

    /**
     * Saves a list of musicians in the JSON file provided for this purpose.
     * @param musicians List to be saved.
     */
    private saveMusicians(musicians: Musician[]) {
        fs.writeFile('./app/data/MusiciansData.json', JSON.stringify(musicians), (err) => {
            console.log(err);
        });
    }

    /**
     * Create a musician object from the information in a form.
     * @param formData Completed form
     * @return Return a new musician if the information allows it. Returns null otherwise.
     */
    public getMusicianFromFormData(formData: FormData): Musician | null {
        let newMusician = null;
        // Init arrays
        let instrumentsPlayed: Instruments[] = [];
        let stylesPlayed: MusicStyle[] = [];
        // Retrieving values
        let name = formData.get("name");
        let instrumentsChecked = formData.getAll("instrument-checkbox");
        let stylesChecked = formData.getAll("music-style-checkbox");
        let id = formData.get("musician-id");
        // Processing of checked instruments
        for(let i = 0; i < instrumentsChecked.length; i++) {
            instrumentsPlayed.push(parseInt(instrumentsChecked[i].toString()));
        }
        // Processing of checked music styles
        for(let i = 0; i < stylesChecked.length; i++) {
            stylesPlayed.push(parseInt(stylesChecked[i].toString()));
        }
        // Creates an id if there is not
        if(id === null) {
            id = new Date().getTime().toString();
        }
        // Creates a musician and adds him to the list if all fields are filled in.
        if(name !== null) {
            newMusician = {
                id: id.toString(),
                name: name.toString(),
                instruments: instrumentsPlayed,
                styles: stylesPlayed
            };
        }
        return newMusician;
    }
}

export default new MusicianService();

export async function getMusicians(style: string | null, instrument: string | null): Promise<Array<Musician>> {
    const service = new MusicianService();
    let filteredStyle: Musician[] = [];
    let filteredInstrument: Musician[] = [];
    let musicians: Musician[] = [];
    if(style === null && instrument === null) {
        musicians = service.getAllMusicians();
    } else {
        if(style) {
            filteredStyle = service.getAllMusicians().filter((musician) => {
                let isSameStyle = false;
                for(let i = 0; i < musician.styles.length; i++) {
                    if(musician.styles[i] === parseInt(style)) {
                        isSameStyle = true;
                    }
                }
                return isSameStyle;
            });
        }
        if(instrument) {
            filteredInstrument = service.getAllMusicians().filter((musician) => {
                let isSameInstrument = false;
                for(let i = 0; i < musician.instruments.length; i++) {
                    if(musician.instruments[i] === parseInt(instrument)) {
                        isSameInstrument = true;
                    }
                }
                return isSameInstrument;
            });
        }
        musicians = filteredStyle.concat(filteredInstrument);
    }
    return musicians;
}

export async function getMusician(id: string): Promise<Musician | null> {
    const service = new MusicianService();
    return service.getMusicianById(id);
}
