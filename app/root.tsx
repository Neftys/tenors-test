import styles from "./styles/app.css"
import Header from "~/components/Header";
import {Outlet} from "react-router";
import { Links } from "@remix-run/react";

export function links() {
    return [{ rel: "stylesheet", href: styles }]
}

export default function App() {
  return (
    <html lang="fr">
      <head>
          <Links/>
          <title>Tenors Opera</title>
          <link rel="icon" type="image/png" href="/assets/images/chat.png"/>
      </head>
      <body>
        <Header/>
        <Outlet/>
      </body>
    </html>
  );
}
