import React from "react";
import {MusicStyle, READABLE_MUSIC_STYLE} from "~/models/MusicStyle";

/**
 * Part of the form to manage a concert. Lists all the music styles and allows to select those played.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class SelectConcertMusicStyle extends React.Component<{
    /**
     * Music styles played during the concert.
     * Allows to select the appropriate choice when launching the component.
     */
    currentMusicStyle?: MusicStyle
}> {

    render() {

        return (
            <div>
                <label
                    htmlFor="music-style"
                    className="block text-gray-700 text-sm font-bold mb-2"
                >
                    Choisissez un style de musique
                </label>
                <select
                    id="music-style"
                    name="music-style"
                    className="w-full form-select rounded-lg border border-gray-200"
                >
                    <option value={undefined} selected={this.props.currentMusicStyle === undefined}>Choisir un style musical</option>
                    {
                        READABLE_MUSIC_STYLE.map((style, index) => (
                            <option
                                key={index}
                                value={index}
                                selected={this.props.currentMusicStyle === index}
                            >
                                {style}
                            </option>
                        ))
                    }
                </select>

            </div>
        );

    }

}

export default SelectConcertMusicStyle;