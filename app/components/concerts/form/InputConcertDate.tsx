import React from "react";
import UtilsService from "~/services/UtilsService";

/**
 * Part of the form to manage a concert. Allows you to select the concert date.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class InputConcertDate extends React.Component<{
    /**
     * Date already filled in. Allows to pre-fill the field.
     */
    currentDate?: Date
}> {

    render() {

        /**
         * Format the date so that it can be displayed in the input.
         */
        const getDefaultValue = () => {
            return this.props.currentDate === undefined ? undefined : UtilsService.formatDateForInput(this.props.currentDate);
        }

        return (
            <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="date">
                    Date de l'évènement
                </label>
                <input
                    className="w-full form-input rounded-lg border border-gray-200"
                    id="date"
                    name="date"
                    type="datetime-local"
                    placeholder="Date"
                    defaultValue={getDefaultValue()}
                />
            </div>
        );

    }

}

export default InputConcertDate;