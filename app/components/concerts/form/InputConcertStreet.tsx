import React from "react";

/**
 * Part of the form to manage a concert. Allows you to select the concert street.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class InputConcertStreet extends React.Component<{
    /**
     * Street already filled in. Allows to pre-fill the field.
     */
    currentStreet?: string
}> {

    render() {

        return (
            <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="street">
                    Rue de l'évènement
                </label>
                <input
                    className="w-full form-input rounded-lg border border-gray-200"
                    id="street"
                    name="street"
                    type="text"
                    placeholder="Rue"
                    defaultValue={this.props.currentStreet}
                />
            </div>
        );

    }

}

export default InputConcertStreet;