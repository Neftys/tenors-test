import React from "react";
import {City, READABLE_CITIES} from "~/models/City";

/**
 * Part of the form to manage a concert. Allows you to select the concert city.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class SelectConcertCity extends React.Component<{
    /**
     * City in which the concert takes place.
     * Allows to select the appropriate choice when launching the component.
     */
    currentCity?: City
}> {

    render() {

        return (
            <div>
                <label
                    htmlFor="city"
                    className="block text-gray-700 text-sm font-bold mb-2"
                >
                    Choisissez une ville
                </label>
                <select
                    id="city"
                    name="city"
                    className="w-full form-select rounded-lg border border-gray-200"
                >
                    <option selected={this.props.currentCity === undefined}>Choisir une ville</option>
                    {
                        READABLE_CITIES.map((city, index) => (
                            <option
                                key={index}
                                value={index}
                                selected={this.props.currentCity === index}
                            >
                                {city}
                            </option>
                        ))
                    }
                </select>

            </div>
        );

    }

}

export default SelectConcertCity;