import React from "react";
import {Form} from "@remix-run/react";
import SelectConcertCity from "~/components/concerts/form/SelectConcertCity";
import SelectConcertMusicStyle from "~/components/concerts/form/SelectConcertMusicStyle";
import InputConcertStreet from "~/components/concerts/form/InputConcertStreet";
import InputConcertDate from "~/components/concerts/form/InputConcertDate";

/**
 * Form to add a new concert.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class AddConcertPage extends React.Component<{}> {

    render() {

        return (
            <div className="container w-1/3 mx-auto">
                <h1 className="text-4xl font-bold tracking-wider my-10 text-center">Nouveau concert</h1>
                <div className="w-full mx-auto">
                    <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="post">

                        <InputConcertDate/>
                        <InputConcertStreet/>
                        <SelectConcertCity/>
                        <SelectConcertMusicStyle/>

                        <button
                            className="w-full rounded-md mx-auto border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700"
                            type="submit"
                        >
                            Ajouter
                        </button>
                    </Form>
                </div>
            </div>
        );
    }

}

export default AddConcertPage;