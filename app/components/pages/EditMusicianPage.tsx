import React from "react";
import {Form, useLoaderData} from "@remix-run/react";
import InputMusicianName from "~/components/musicians/form/InputMusicianName";
import InputMusicianInstruments from "~/components/musicians/form/InputMusicianInstruments";
import InputMusicianStyles from "~/components/musicians/form/InputMusicianStyles";
import {LoaderData} from "~/routes/musicians/edit-musician/$musicianId";

/**
 * Page allowing to modify a musician via a form.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
export default function EditMusicianPage() {

    // Retrieves the musician according to its identifier passed in parameter of the route.
    const {musician} = useLoaderData() as LoaderData;

    return (
        <div className="container w-1/3 mx-auto">
            <h1 className="text-4xl font-bold tracking-wider my-10 text-center">Modifier musicien</h1>
            <div className="w-full mx-auto">
                <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="post">

                    <input hidden defaultValue={musician?.id} name="musician-id"/>
                    <InputMusicianName currentName={musician?.name}/>
                    <InputMusicianInstruments currentInstruments={musician?.instruments}/>
                    <InputMusicianStyles currentStyles={musician?.styles}/>

                    <button
                        className="w-full rounded-md mx-auto border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700"
                        type="submit">
                        Modifier
                    </button>
                </Form>
            </div>
        </div>
    );

}
