import React from "react";
import {Form, useLoaderData} from "@remix-run/react";
import InputConcertDate from "~/components/concerts/form/InputConcertDate";
import InputConcertStreet from "~/components/concerts/form/InputConcertStreet";
import SelectConcertCity from "~/components/concerts/form/SelectConcertCity";
import SelectConcertMusicStyle from "~/components/concerts/form/SelectConcertMusicStyle";
import {LoaderData} from "~/routes/concerts/edit-concert/$concertId";

/**
 * Page allowing to modify a concert via a form.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
export default function EditConcertPage() {

    // Retrieves the concert according to its identifier passed in parameter of the route.
    const {concert} = useLoaderData() as LoaderData;

    return (
        <div className="container w-1/3 mx-auto">
            <h1 className="text-4xl font-bold tracking-wider my-10 text-center">Modifier concert</h1>
            <div className="w-full mx-auto">
                <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="post">

                    <input hidden defaultValue={concert?.id} name="concert-id"/>
                    <InputConcertDate currentDate={concert?.date}/>
                    <InputConcertStreet currentStreet={concert?.street}/>
                    <SelectConcertCity currentCity={concert?.city}/>
                    <SelectConcertMusicStyle currentMusicStyle={concert?.style}/>

                    <button
                        className="w-full rounded-md mx-auto border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700"
                        type="submit">
                        Modifier
                    </button>
                </Form>
            </div>
        </div>
    );

}