import React from "react";
import {Form, useLoaderData} from "@remix-run/react";
import {Link} from "react-router-dom";
import type {Concert} from "~/models/Concert";
import ConcertCard from "../concerts/ConcertCard";
import type {LoaderData} from "~/routes/concerts/all";
import SelectConcertMusicStyle from "~/components/concerts/form/SelectConcertMusicStyle";

/**
 * Page displayed when the /concerts route is used.
 * @author Krysalizys
 * @creation_date 02/11/2022
 **/
export default function ConcertPage() {

    const { futuresConcerts, pastConcerts } = useLoaderData() as unknown as LoaderData;

    return (
        <>
            <div className="w-full text-center flex flex-col justify-center items-center pt-6 gap-y-10">
                <Link
                    to="../add-concert"
                    className="w-1/3 inline-flex items-center justify-center gap-x-3 rounded-md mx-auto border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700"
                >
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                    </svg>
                    Ajouter
                </Link>
                <Form method="get">
                    <div className="mx-auto flex items-center gap-x-5">
                        <SelectConcertMusicStyle/>
                        <button className="inline-flex items-center justify-center gap-x-3 rounded-md border border-transparent bg-green-600 px-5 py-2 text-base font-medium text-white hover:bg-green-800">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12.75l6 6 9-13.5" />
                            </svg>
                            Valider
                        </button>
                        <Link
                            to="../all"
                            className="inline-flex items-center justify-center gap-x-3 rounded-md border border-transparent bg-red-600 px-5 py-2 text-base font-medium text-white hover:bg-red-700"
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                            </svg>
                            Réinitialiser
                        </Link>
                    </div>
                </Form>
                <h1 className="text-4xl font-bold tracking-wider">Concerts à venir</h1>
                <div className="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-20">
                    {
                        futuresConcerts.map((concert: Concert) => (
                            <ConcertCard concert={concert} key={concert.id}/>
                        ))
                    }
                </div>
                <h1 className="text-4xl font-bold tracking-wider">Concerts passés</h1>
                <div className="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-20">
                    {
                        pastConcerts.map((concert: Concert) => (
                            <ConcertCard concert={concert} key={concert.id}/>
                        ))
                    }
                </div>


            </div>

        </>
    );
}