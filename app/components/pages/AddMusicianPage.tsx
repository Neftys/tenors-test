import React from "react";
import InputMusicianName from "~/components/musicians/form/InputMusicianName";
import InputMusicianInstruments from "~/components/musicians/form/InputMusicianInstruments";
import InputMusicianStyles from "~/components/musicians/form/InputMusicianStyles";
import {Form} from "@remix-run/react";


/**
 * Form to add a new musician.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
class AddMusicianPage extends React.Component<{}> {

    render() {

        return (
            <div className="container w-1/3 mx-auto">
                <h1 className="text-4xl font-bold tracking-wider my-10 text-center">Nouveau musicien</h1>
                <div className="w-full mx-auto">
                    <Form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="post">

                        <InputMusicianName/>
                        <InputMusicianInstruments/>
                        <InputMusicianStyles/>

                        <button
                            className="w-full rounded-md mx-auto border border-transparent bg-indigo-600 px-5 py-3 text-base font-medium text-white hover:bg-indigo-700"
                            type="submit">
                            Ajouter
                        </button>
                    </Form>
                </div>
            </div>
        );

    }

}

export default AddMusicianPage;
