import React from "react";
import type {Musician} from "~/models/Musician";
import {READABLE_INSTRUMENTS} from "~/models/Instruments";
import {READABLE_MUSIC_STYLE} from "~/models/MusicStyle";
import DeleteButton from "~/components/musicians/DeleteButton";
import {Link} from "@remix-run/react";

/**
 * Component displaying a card listing the information of a musician.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
class MusicianCard extends React.Component<{
    /**
     * Musician to display.
     */
    musician: Musician
}> {

    render() {
        return (
            <div className="overflow-hidden shadow sm:rounded-lg">
                <div className="flex justify-between items-center px-4 py-5 sm:px-6">
                    <div>
                        <h3 className="text-lg font-medium leading-6 text-gray-900">
                            {this.props.musician.name}
                        </h3>
                        <p className="mt-1 max-w-2xl text-sm text-gray-500">
                            {this.props.musician.id}
                        </p>
                    </div>
                    <div className="w-3/5 flex justify-between items-center">
                        <DeleteButton musicianId={this.props.musician.id}/>
                        <Link
                            to={"../edit-musician/" + this.props.musician.id}
                            className="flex gap-x-2 rounded-md border border-green-600 px-5 py-3 text-base font-medium text-green-600 hover:bg-green-600 hover:text-white"
                            type="button"
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125" />
                            </svg>

                            Modifier
                        </Link>
                    </div>
                </div>
                <div className="border-t border-gray-200">
                    <dl>
                        <div className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt className="text-sm font-medium text-gray-500">Instruments</dt>
                            <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                                {
                                    // List the instruments played by the musician.
                                    this.props.musician.instruments.map((instrument) => (
                                        <span key={instrument}> {READABLE_INSTRUMENTS[instrument]} - </span>
                                    ))
                                }
                            </dd>
                        </div>
                        <div className="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt className="text-sm font-medium text-gray-500">Styles</dt>
                            <dd className="mt-1 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                                {
                                    // List the styles of music practiced by the musician.
                                    this.props.musician.styles.map((style) => (
                                        <span key={style}> {READABLE_MUSIC_STYLE[style]} - </span>
                                    ))
                                }
                            </dd>
                        </div>
                    </dl>
                </div>
            </div>
        );

    }

}

export default MusicianCard;