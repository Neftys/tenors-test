import React from "react";
import {READABLE_INSTRUMENTS} from "~/models/Instruments";

/**
 * Part of the form to manage a musician. Lists all the instruments and allows to select those played.
 * @author Krysalizys
 * @creation_date 05/11/2022
 **/
class SelectMusicianInstruments extends React.Component<{}> {

    render() {

        return (
            <div>
                <label
                    htmlFor="music-style"
                    className="block text-gray-700 text-sm font-bold mb-2"
                >
                    Choisissez un instrument
                </label>
                <select
                    id="instrument"
                    name="instrument"
                    className="w-full form-select rounded-lg border border-gray-200"
                >
                    <option value={undefined}>Choisir un instrument</option>
                    {
                        READABLE_INSTRUMENTS.map((instrument, index) => (
                            <option
                                key={index}
                                value={index}
                            >
                                {instrument}
                            </option>
                        ))
                    }
                </select>

            </div>
        );

    }

}

export default SelectMusicianInstruments;