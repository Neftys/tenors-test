import React from "react";
import {MusicStyle, READABLE_MUSIC_STYLE} from "~/models/MusicStyle";

/**
 * Part of the form to manage a musician. Lists all the music styles and allows to check those played.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
class InputMusicianStyles extends React.Component<{
    /**
     * Music styles played by the musician. Allows to check the appropriate checkboxes when launching the component.
     */
    currentStyles?: MusicStyle[],
}> {

    render() {

        /**
         * Tells if the value provided is in the list of music styles passed in props.
         * @param value Value that we want to compare to the list.
         */
        const isValueInCurrentStyle = (value: number): boolean => {
            let isValueInCurrentStyle = false;
            if(this.props.currentStyles !== undefined) {
                for (let i = 0; i < this.props.currentStyles.length; i++) {
                    if(value === this.props.currentStyles[i]) {
                        isValueInCurrentStyle = true;
                    }
                }
            }
            return isValueInCurrentStyle;
        }

        return (
            <div className="mb-4">
                <h3 className="block text-gray-700 text-sm font-bold mb-2">
                    Styles musicaux
                </h3>
                <ul className="text-sm font-medium bg-white rounded-lg border border-gray-200">
                    {
                        // Scans all available music styles to display a checkbox.
                        READABLE_MUSIC_STYLE.map((music, index) => (
                            <li key={index} className="w-full rounded-t-lg border-b border-gray-200">
                                <div className="flex items-center pl-3">
                                    <input
                                        id="music-style-checkbox"
                                        name="music-style-checkbox"
                                        type="checkbox"
                                        value={index}
                                        className="form-checkbox text-indigo-700 rounded border-gray-300 focus:ring-blue-500"
                                        defaultChecked={isValueInCurrentStyle(index)}
                                    />
                                    <label
                                        htmlFor="music-style-checkbox"
                                        className="py-3 ml-2 w-full text-sm font-medium"
                                    >
                                        {music}
                                    </label>
                                </div>
                            </li>
                        ))
                    }
                </ul>
            </div>
        );

    }

}

export default InputMusicianStyles;