import React from "react";
import {Instruments, READABLE_INSTRUMENTS} from "~/models/Instruments";

/**
 * Part of the form to manage a musician. Lists all the instruments and allows to check those played.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
class InputMusicianInstruments extends React.Component<{
    /**
     * Instruments played by the musician. Allows to check the appropriate checkboxes when launching the component.
     */
    currentInstruments?: Instruments[],
}> {

    render() {

        /**
         * Tells if the value provided is in the list of instruments passed in props.
         * @param value Value that we want to compare to the list.
         */
        const isValueInCurrentInstrument = (value: number): boolean => {
            let isValueInCurrentInstruments = false;
            if(this.props.currentInstruments !== undefined) {
                for (let i = 0; i < this.props.currentInstruments.length; i++) {
                    if(value === this.props.currentInstruments[i]) {
                        isValueInCurrentInstruments = true;
                    }
                }
            }
            return isValueInCurrentInstruments;
        }


        return (
            <div className="mb-4">
                <h3 className="block text-gray-700 text-sm font-bold mb-2">
                    Instruments
                </h3>
                <ul className="text-sm font-medium bg-white rounded-lg border border-gray-200">
                    {
                        // Scans all available instruments to display a checkbox.
                        READABLE_INSTRUMENTS.map((instrument, index) => (
                            <li key={index} className="w-full rounded-t-lg border-b border-gray-200">
                                <div className="flex items-center pl-3">
                                    <input
                                        id="instrument-checkbox"
                                        name="instrument-checkbox"
                                        type="checkbox"
                                        value={index}
                                        className="form-checkbox text-indigo-700 rounded border-gray-300 focus:ring-blue-500"
                                        defaultChecked={isValueInCurrentInstrument(index)}
                                    />
                                    <label
                                        htmlFor="instrument-checkbox"
                                        className="py-3 ml-2 w-full text-sm font-medium"
                                    >
                                        {instrument}
                                    </label>
                                </div>
                            </li>
                        ))
                    }
                </ul>
            </div>
        );

    }

}

export default InputMusicianInstruments;