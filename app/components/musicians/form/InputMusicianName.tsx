import React from "react";

/**
 * Part of the form to manage a musician. Allows you to enter a scene name.
 * @author Krysalizys
 * @creation_date 04/11/2022
 **/
class InputMusicianName extends React.Component<{
    /**
     * Name already filled in. Allows to pre-fill the field.
     */
    currentName?: string,
}> {

    render() {

        return (
            <div className="mb-4">
                <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="name">
                    Nom de scène
                </label>
                <input
                    className="w-full form-input rounded-lg border border-gray-200"
                    id="name"
                    name="name"
                    type="text"
                    placeholder="Nom de scène"
                    defaultValue={this.props.currentName}
                />
            </div>
        );

    }

}

export default InputMusicianName;